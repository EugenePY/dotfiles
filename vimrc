"""pathogen set up 

filetype plugin indent on
execute pathogen#infect()
execute pathogen#helptags()
filetype plugin indent on
syntax on

"""

inoremap {      {}<Left>
inoremap {<CR>  {<CR>}<Esc>O
inoremap {{     {
inoremap {}     {}


""" set the clipboard
set clipboard+=unnamed

""" set backspace
set backspace=indent,eol,start

"""python mode setup
"override go-to.definition key shortcut to Ctrl-]
" Override run current python file key shortcut to Ctrl-Shift-e
"
let g:pymode_run_bind = "<C-S-e>"

" " Override view python doc key shortcut to Ctrl-Shift-d
let g:pymode_doc_bind = "<C-S-d>"

" " Enable the pymode motion
let g:pymode_motion = 1

" " Enable the pymode check point
let g:pymode_breakpoint = 1

" " Turn on code checking
let g:pymode_lint = 1
let g:pymode_rope = 0

""" jedi-autocomlete



""" enable virtualenv

let g:pymode_virtualenv = 1

""" enable rope auto-complete
let g:pymode_rope_completion = 1



"""NERDss tree setup
map <C-l> :silent! NERDTreeToggle<CR>


" " UltiSnips setup
let g:UltiSnipsExpandTrigger='<C-k>'
let g:UltiSnipsJumpForwardTrigger='<C-k>'
let g:UltiSnipsJumpBackwardTrigger='<C-s-k>'


""" 
set runtimepath+=~/.vim/ultisnips_rep

set number

syntax enable

set tabstop=4

set relativenumber

inoremap jk <esc>

set t_Co=256

set hlsearch

set smartindent 


"""Tab mapping 
map <C-T> :tabnew <CR> 
inoremap <C-T> L :tabn <CR>
inoremap <C-T> H :tabp <CR>

set clipboard=unnamed


map <c-f> :call JsBeautify()<cr>
"" or
autocmd FileType javascript noremap <buffer>  <c-f> :call JsBeautify()<cr>
" for json
autocmd FileType json noremap <buffer> <c-f> :call JsonBeautify()<cr>
" " for jsx
autocmd FileType jsx noremap <buffer> <c-f> :call JsxBeautify()<cr>
" " for html
autocmd FileType html noremap <buffer> <c-f> :call HtmlBeautify()<cr>
" " for css or scss
autocmd FileType css noremap <buffer> <c-f> :call CSSBeautify()<cr>

"" Syntatic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

"" clojure, linter etc
let g:syntastic_clojure_checkers = ['clj-kondo']
let g:rainbow_active = 1
let g:paredit_mode = 1
