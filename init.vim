""" baic configuration

set number
syntax enable

set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab

set relativenumber
inoremap jk <esc>
set hlsearch
set smartindent 
set backspace=indent,eol,start
let mapleader=" "
set colorcolumn=80

set wrap 
set linebreak

" yank to clipboard
if has("clipboard")
  set clipboard=unnamed " copy to the system clipboard

  if has("unnamedplus") " X11 support
    set clipboard+=unnamedplus
  endif
endif

""" some remaps
nnoremap Y y$
nnoremap n nzzzv
nnoremap N Nzzzv

""" move text
vnoremap J :m '>+1<CR>gv=gv
nnoremap <leader>k :m .-2<CR>==
nnoremap <leader>j :m .+1<CR>==

"""multiline comment"""
nnoremap cc @='I#<C-V><Esc>j'<CR>

""" Jump and recenter
nnoremap <c-o> <c-o>zz nnoremap <c-l> <c-l>zz

"""window movement"""
nnoremap <leader>0 <C-w>1w
nnoremap <leader>1 <C-w>2w
nnoremap <leader>2 <C-w>3w
nnoremap <leader>3 <C-w>4w
nnoremap <leader>4 <C-w>5w

""" clipboard
nnoremap <leader>p "+p
vnoremap <leader>p "+p
nnoremap <leader>P "+P
vnoremap <leader>P "+P
nnoremap <leader>y "+y
vnoremap <leader>y "+y
nnoremap <leader>Y "+y$

"""remap the exit"""
nnoremap zx :qa<CR>

""" PLUG-INs
call plug#begin('~/dotfiles/nvim/site/autoload')

""" telescope
Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'

""" treesitter
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

""" files
Plug 'preservim/nerdtree'

""" status bar
" If you want to have icons in your statusline choose one of these
Plug 'vim-airline/vim-airline'

""" bracket coloring
Plug 'luochen1990/rainbow'

"" LSP
Plug 'neovim/nvim-lspconfig'

""" nvim-cmp
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/nvim-cmp'

""" debuger
Plug 'mfussenegger/nvim-dap'

"" Color Scheme
Plug 'morhetz/gruvbox'

""" formator 
Plug 'sbdchd/neoformat'
Plug 'junegunn/vim-easy-align'

" For vsnip users.
Plug 'hrsh7th/cmp-vsnip'
Plug 'hrsh7th/vim-vsnip'

" For luasnip users.
Plug 'L3MON4D3/LuaSnip', {'tag': 'v<CurrentMajor>.*'}
Plug 'saadparwaiz1/cmp_luasnip'
"" some snippets
Plug 'rafamadriz/friendly-snippets'
" For ultisnips users.
" Plug 'SirVer/ultisnips'
" Plug 'quangnguyen30192/cmp-nvim-ultisnips'

" For snippy users.
" Plug 'dcampos/nvim-snippy'
" Plug 'dcampos/cmp-snippy'

call plug#end()

"""NerdTree setup"""
map <leader>l :silent! NERDTreeToggle<CR>

"""rainbow setting
let g:rainbow_active = 1

"""theme"""
autocmd vimenter * ++nested colorscheme gruvbox
" transparent bg
autocmd vimenter * hi Normal guibg=NONE ctermbg=NONE
" For Vim<8, replace EndOfBuffer by NonText
autocmd vimenter * hi EndOfBuffer guibg=NONE ctermbg=NONE

""" Treesitter

lua <<EOF
require'nvim-treesitter.configs'.setup {
  highlight = {
    enable = true,
    -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
    -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
    -- Using this option may slow down your editor, and you may see some duplicate highlights.
    -- Instead of true it can also be a list of languages
    additional_vim_regex_highlighting = false,
  },
}
EOF

"""Telescope"""
nnoremap <leader>ff <cmd>lua require('telescope.builtin').find_files()<cr>
nnoremap <leader>fg <cmd>lua require('telescope.builtin').live_grep()<cr>
nnoremap <leader>fb <cmd>lua require('telescope.builtin').buffers()<cr>
nnoremap <leader>fh <cmd>lua require('telescope.builtin').help_tags()<cr> " airline

""" new live grep, specify the search path
command! -nargs=1 -complete=file_in_path TeleLiveGrep lua require('telescope.builtin').live_grep({search_dirs={<f-args>},})<cr>

" airline
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#buffer_nr_show = 1

""" Lsp Compleletion
" Use <Tab> and <S-Tab> to navigate through popup menu
inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
" Set completeopt to have a better completion experience
set completeopt=menuone,noinsert,noselect
" Avoid showing message extra message when using completion
set shortmess+=c

nnoremap <silent>gr <cmd>lua require('lspsaga.rename').rename()<CR>

""" Formattor
nnoremap <leader>f <cmd>Neoformat<CR>

""" LuaSnips
imap <silent><expr> <Tab> luasnip#expand_or_jumpable() ? '<Plug>luasnip-expand-or-jump' : '<Tab>' 
" -1 for jumping backwards.
inoremap <silent> <S-Tab> <cmd>lua require'luasnip'.jump(-1)<Cr>

snoremap <silent> <Tab> <cmd>lua require('luasnip').jump(1)<Cr>
snoremap <silent> <S-Tab> <cmd>lua require('luasnip').jump(-1)<Cr>

" For changing choices in choiceNodes (not strictly necessary for a basic setup).
imap <silent><expr> <C-E> luasnip#choice_active() ? '<Plug>luasnip-next-choice' : '<C-E>'
smap <silent><expr> <C-E> luasnip#choice_active() ? '<Plug>luasnip-next-choice' : '<C-E>'
 

""" lua.configs 
lua <<EOF
require'init'
EOF
