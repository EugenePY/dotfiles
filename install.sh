#!/bin/bash

dir=~/dotfiles                   	 	 			 # dotfiles directory
olddir=~/dotfiles_old            	 	 			 # old dotfiles backup directory
files=(zshrc vimrc vim tmux.conf init.vim nvim) # list of files/folders to symlink in homedir

##########
echo 'Create $olddir directory for backupof any existing dotfile in ~'
mkdir -p $olddir
echo 'Done ....'

######### change to the new dotfile dir 
echo 'Create $dir in ~ for new dotfile'
mkdir $dir
cd $dir
echo 'Done ....'

######## Create the symlink
for file in $files[@]; do
	echo "Moving any existing dotfiles from ~ to $olddir"
	mv ~/.$file ~/dotfiles_old/
	echo "Creating symlink to $file in home directory."
	ln -s $dir/$file ~/.$file
done

