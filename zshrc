# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh
export SHELL=/bin/zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each

# time that oh-my-zsh is loaded.
ZSH_THEME="mrtazz"

# zsh-completions
fpath=(/usr/local/share/zsh-completions $fpath)

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git, sftp, tmux)

source $ZSH/oh-my-zsh.sh
# You may need to manually set your language environment
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

#Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi
#
#
 export NVM_DIR="$HOME/.nvm"
  [ -s "/usr/local/opt/nvm/nvm.sh" ] && \. "/usr/local/opt/nvm/nvm.sh"  # This loads nvm
  [ -s "/usr/local/opt/nvm/etc/bash_completion.d/nvm" ] && \. "/usr/local/opt/nvm/etc/bash_completion.d/nvm"  # This loads nvm bash_completion

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.

if [ -n "$INSIDE_EMACS" ]; then
	export ZSH_THEME="rawsyntax"
else
	export ZSH_THEME="robbyrussell"
fi

# JAVA
#export JAVA_HOME="$(/usr/libexec/java_home)"
#export PATH=$HOME/Documents/Java/apache-maven-3.3.9/bin:$SPARK_HOME/bin:`npm bin`
#export MV_HOME=/Users/bigtreehouse/Documents/Java/apache-maven-3.3.9

# Nvidia GPU
#export PATH=/Developer/NVIDIA/CUDA-7.5/bin:$PATH
#export DYLD_LIBRARY_PATH=/Developer/NVIDIA/CUDA-7.5/lib:$DYLD_LIBRARY_PATH

# JULIA PATH
#export JULIA_PATH="/Applications/Julia-0.6.app/Contents/Resources/julia/bin"

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
#export PATH="$PATH:$HOME/.rvm/bin":/usr/local/mysql/bin:$JULIA_PATH
##alias chrome="/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome"
#alias chrome-canary="/Applications/Google\ Chrome\ Canary.app/Contents/MacOS/Google\ Chrome\ Canary"
#alias chromium="/Applications/Chromium.app/Contents/MacOS/Chromium"

## EMACS
#alias emacs="emacs -nw"
#alias emacs-window="emacs"
#export PATH="/usr/local/Cellar/llvm/12.0.1/bin:/usr/local/opt/openssl/bin:$PATH"

# Javascript
#export NVM_DIR="$HOME/.nvm"
#[ -s "/usr/local/opt/nvm/nvm.sh" ] && . "/usr/local/opt/nvm/nvm.sh"  # This loads nvm
#[ -s "/usr/local/opt/nvm/etc/bash_completion.d/nvm" ] && . "/usr/local/opt/nvm/etc/bash_completion.d/nvm"  # This loads nvm bash_completion
#export PATH="/usr/local/opt/openssl@1.1/bin:$PATH"
#
export PATH="/usr/local/opt/llvm/bin:$PATH"
# pyenv
export PYENV_ROOT="$HOME/.pyenv"
eval "$(pyenv init --path)"

#alias luamake=/Users/bigtreehouse/lib/lua-language-server/3rd/luamake/luamake

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/Users/bigtreehouse/miniconda3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/Users/bigtreehouse/miniconda3/etc/profile.d/conda.sh" ]; then
        . "/Users/bigtreehouse/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="/Users/bigtreehouse/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<
